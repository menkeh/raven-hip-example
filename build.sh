#!/usr/bin/env bash

set -eu

. ./Modulefile

export CC=gcc
export CXX=g++
export CXXFLAGS="-Wall -Wextra -Wpedantic"
cmake -S . -B _build/ -DCMAKE_MODULE_PATH="$HIP_HOME/lib64/cmake/hip" -DHIP_ROOT_DIR="$HIP_HOME" -Wno-deprecated
cmake --build _build/
