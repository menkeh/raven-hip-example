#include <hip/hip_runtime.h>
#include <hip/hiprtc.h>

#include <cstdlib>
#include <iostream>
#include <vector>

#define HIP_SAFE_CALL(x) hip_safe_call((x), #x, __FILE__, __LINE__)
#define HIPRTC_SAFE_CALL(x) hiprtc_safe_call((x), #x, __FILE__, __LINE__)

static hipError_t hip_safe_call(hipError_t err, const char *cmd, const char *file, int line) {
    if (err != hipSuccess) {
        std::cerr << file << ":" << line << ": " << hipGetErrorName(err) << " (error code " << err << ")\n"
                  << "    " << cmd << "\n"
                  << "    ^~~~~~~~~~~~~~~~\n"
                  << hipGetErrorString(err) << std::endl;
        std::exit(EXIT_FAILURE);
    }
    return err;
}

static hiprtcResult hiprtc_safe_call(hiprtcResult err, const char *cmd, const char *file, int line) {
    if (err != HIPRTC_SUCCESS) {
        std::cerr << file << ":" << line << ": error code " << err << "\n"
                  << "    " << cmd << "\n"
                  << "    ^~~~~~~~~~~~~~~~\n"
                  << hiprtcGetErrorString(err) << std::endl;
        std::exit(EXIT_FAILURE);
    }
    return err;
}

int main() {
    hiprtcProgram prog;

    int n_devices = 0;
    HIP_SAFE_CALL(hipGetDeviceCount(&n_devices));
    if (n_devices == 0) {
        std::cerr << "No accelerator devices" << std::endl;
        return EXIT_FAILURE;
    }

    hipDevice_t device;
    HIP_SAFE_CALL(hipDeviceGet(&device, 0));
    hipCtx_t ctx;
    HIP_SAFE_CALL(hipCtxCreate(&ctx, 0, device));

    HIPRTC_SAFE_CALL(hiprtcCreateProgram(&prog, R"~~(
extern "C" __global__ void gpu_kernel(unsigned int *value) {
    value[0] = 1;
#ifdef __HIP_PLATFORM_HCC__
    value[1] = 1;
#endif
#ifdef __HIP_PLATFORM_AMD__
    value[2] = 1;
#endif
#ifdef __HIP_PLATFORM_NVCC__
    value[3] = 1;
#endif
#ifdef __HIP_PLATFORM_NVIDIA__
    value[4] = 1;
#endif
#ifdef CUSTOM_STUFF
    value[5] = 1;
#endif
}
    )~~",
            "kernel.cl", 0, NULL, NULL));

    const char *options[] = { "-DCUSTOM_STUFF=1" };
    HIPRTC_SAFE_CALL(hiprtcCompileProgram(prog, 1, options));

    size_t codeSize;
    HIPRTC_SAFE_CALL(hiprtcGetCodeSize(prog, &codeSize));

    std::vector<char> kernel_binary(codeSize);
    HIPRTC_SAFE_CALL(hiprtcGetCode(prog, kernel_binary.data()));

    HIPRTC_SAFE_CALL(hiprtcDestroyProgram(&prog));

    hipModule_t module;
    hipFunction_t kernel;

    HIP_SAFE_CALL(hipModuleLoadData(&module, kernel_binary.data()));
    HIP_SAFE_CALL(hipModuleGetFunction(&kernel, module, "gpu_kernel"));

    unsigned int arg[] = {0, 0, 0, 0, 0, 0};
    hipDeviceptr_t darg;
#if defined(__HIP_PLATFORM_NVIDIA__) && __HIP_PLATFORM_NVIDIA__
    HIP_SAFE_CALL(hipMalloc(reinterpret_cast<void**>(&darg), sizeof(arg)));
#else
    HIP_SAFE_CALL(hipMalloc(&darg, sizeof(arg)));
#endif
    HIP_SAFE_CALL(hipMemcpyHtoD(darg, arg, sizeof(arg)));

    void *args[1] = { &darg };
    HIP_SAFE_CALL(hipModuleLaunchKernel(kernel,
            1, 1, 1, // grid
            1, 1, 1, // block
            0, // shared memory
            NULL, // stream
            args, // kernelParams
            NULL // extra
    ));

    HIP_SAFE_CALL(hipMemcpyDtoH(arg, darg, sizeof(arg)));
    if (arg[0] == 1) {
        std::cout << "works" << std::endl;
    }
    if (arg[1] == 1) {
        std::cout << "__HIP_PLATFORM_HCC__" << std::endl;
    }
    if (arg[2] == 1) {
        std::cout << "__HIP_PLATFORM_AMD__" << std::endl;
    }
    if (arg[3] == 1) {
        std::cout << "__HIP_PLATFORM_NVCC__" << std::endl;
    }
    if (arg[4] == 1) {
        std::cout << "__HIP_PLATFORM_NVIDIA__" << std::endl;
    }
    if (arg[5] == 1) {
        std::cout << "CUSTOM_STUFF" << std::endl;
    }

    HIP_SAFE_CALL(hipCtxDestroy(ctx));
}
